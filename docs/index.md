# Latest Feed, Files & Data

**Sun Jun 21 17:09:04 2020**

- GeoJSON data [/latest/output-all.json](/latest/output-all.json)
- OSM tags.csv [/latest/tag-report.csv](/latest/tag-report.csv)

## Splitted Sources
- [/latest/great-britain-voltage-points.json](/latest/great-britain-voltage-points.json)
- [/latest/great-britain-voltage-lines.json](/latest/great-britain-voltage-lines.json)
- [/latest/great-britain-voltage-multilinestrings.json](/latest/great-britain-voltage-multilinestrings.json)
- [/latest/great-britain-voltage-multipolygons.json](/latest/great-britain-voltage-multipolygons.json)
- [/latest/great-britain-voltage-other_relations.json](/latest/great-britain-voltage-other_relations.json)
